const activities = (localStorage.getItem('acti') ? JSON.parse(localStorage.getItem('acti')) : []);

function updatels() {
  localStorage.setItem('acti', JSON.stringify(activities));
}
function addActi(todo) {
  activities.push(todo);
  updatels();
}
function removeActi(todo) {
  activities.splice(activities.indexOf(todo), 1);
  updatels();
}

const days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];


const journal = document.getElementById('journal');


const table = document.createElement('table');
const thr = document.createElement('tr');
const thrd = document.createElement('td');

thrd.innerText = 'Ma semaine';
thrd.style.fontWeight = 'bold';

thr.append(thrd);
table.append(thr);

days.forEach((day) => {
  const dayName = document.createElement('td');
  dayName.innerText = day;
  thr.append(dayName);
});
const binCol = document.createElement('td');
binCol.innerHTML = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
thr.append(binCol);
const regex = / /gi;

function deleteRow(event) {
  const row = event.target.parentNode.parentNode;
  row.className += ' displayNone';
}

function addRow(todo) {
  const tr = document.createElement('tr');
  tr.className = todo.replace(regex, '');
  const td = document.createElement('td');
  td.innerText = todo;
  tr.append(td);
  days.forEach((day) => {
    const tdcheck = document.createElement('td');
    const inputDiv = document.createElement('div');
    inputDiv.className += 'centerDiv';
    const radioBtn = document.createElement('input');
    radioBtn.type = 'radio';
    radioBtn.id += todo.replace(regex, '');
    radioBtn.id += day;
    inputDiv.append(radioBtn);
    tdcheck.append(inputDiv);
    tr.append(tdcheck);
  });
  const binCel = document.createElement('td');
  const binBtn = document.createElement('button');
  binBtn.id = todo.replace(regex, '');
  binBtn.innerText = 'x';
  binBtn.addEventListener('click', (e) => {
    deleteRow(e);
    removeActi(todo);
  });
  binCel.append(binBtn);
  tr.append(binCel);
  table.append(tr);
  journal.append(table);
}
// Build a row for each activity in the array
activities.forEach((activity) => {
  addRow(activity);
});
journal.append(table);


// Add an item to the list
const addInput = document.getElementById('add');
const addBtn = document.getElementById('addBtn');
addBtn.addEventListener('click', (e) => {
  const inputText = addInput.value;
  addRow(inputText);
  addActi(inputText);
});
const radiobutton = document.createElement('input');
journal.append(radiobutton);
radiobutton.type = 'radio';
